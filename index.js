import {
  connect
} from 'mongoose'
import bodyParser from 'body-parser'
import express from 'express'
import UserModel from './UserModel'
import cors from 'cors'
import Joi from 'joi'

connect('mongodb+srv://test-user:17L2WY9bUWXqQmn3bnfPg8lB@maincluster.kwdmr.gcp.mongodb.net/yakkyofy?retryWrites=true&w=majority', {
  useNewUrlParser: true
})

// define The User's validatior
const UserSchema = Joi.object({
  email: Joi.string().email().required(),
  password: Joi.string().required(),
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
});

// define The Paggation Helper validatior
const PaggationSchema = Joi.object({
  page: Joi.number().optional().default(1),
  limit: Joi.number().optional().default(10),
})



const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Just a test')
})

// find a given user by mongo ID and update his data;
app.put('/user/:id', async (req, res) => {
  const { id } = req.params
  const { value } = await UserSchema.validate(req.body)
  const result = await UserModel.findByIdAndUpdate(id, value, { new: true }) // set props `new = true` is return the new updated data

  if(!result){
    res.status(404).send(`UserId: ${id} is not found!`)
    return;
  }

  res.send(result);
})

// find a given user by mongo ID and delete it.
app.delete('/user/:id', async (req, res) => {
  const { id } = req.params;
  const user = await UserModel.findById(id)
  if(user){
    const results = await UserModel.deleteOne({ _id: id })
    res.status(202).send(results)
  } else {
    res.status(404).send(`UserId: ${id} is not found!`)
  }
})

app.get('/users',async (req, res) => {
  const { value } = await PaggationSchema.validate(req.query);
  const { page, limit } = value;
  const results = await UserModel.find({},{},{ skip:(page - 1) * limit, limit })
  res.send(results)
})



app.post('/users', (req, res) => {
  let user = new UserModel()

  user.email = req.body.email
  user.firstName = req.body.firstName
  user.lastName = req.body.lastName
  user.password = req.body.password

  user.save((err, newUser) => {
    if (err) res.send(err)
    else res.send(newUser)
  })
})

app.listen(8080, () => console.log('Example app listening on port 8080!'))